<?php
$retina = 2.0;
add_image_size('large', 1600, '', true);
add_image_size('medium', 1100, '', true);
add_image_size('small', 600, '', true);
add_image_size('square', 500, 500, true);
add_image_size('favicon', 64, 64, true);
add_image_size('featured_image', '', 600, true);
add_image_size('archive_project', 354, '', true);
add_image_size('thumbnail', 240, '', true);
add_image_size('services', 360, 240, true);

