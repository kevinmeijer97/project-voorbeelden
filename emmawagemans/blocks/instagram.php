<?php
$instagram_access_token = get_field('instagram_access_token', 'option');
$requestUrl = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" . $instagram_access_token;
$instagramResponse = file_get_contents($requestUrl);
$instagramResponseJson = json_decode($instagramResponse);
$postAmount = get_sub_field('post_amount');
$title = get_sub_field('title');
if($instagramResponseJson != null){
    $instagramPosts = array_slice($instagramResponseJson->data, 0, (!empty($postAmount) ? $postAmount : 6));
    ?>
    <div class="block block-instagram">
        <div class="wrapper">
            <?php
            echo !empty($title) ? "<div class=\"block-instagram-top\"><h2 class=\"title\">{$title}</h2></div>" : "";
            ?>
            <div class="block-instagram-in">
                <?php
                $i = 0;
                foreach($instagramPosts as $instagramPost){
                    $like_count = $instagramPost->likes->count;
                    $comment_count = $instagramPost->comments->count;
                    $text = $instagramPost->caption->text;
                    $text_likes = $like_count > 1 ? get_sub_field('text_likes') : get_sub_field('text_like');
                    $text_comments = $like_count > 1 ? get_sub_field('text_comments') : get_sub_field('text_comment');
                    ?>
                    <div class="item">
                        <a href="<?=$instagramPost->link?>">
                            <img src="<?=$instagramPost->images->standard_resolution->url?>" alt="">
                            <div class="counts">
                                <div class="counts__likes">
                                    <i class="icon icon__likes"></i>
                                    <p><?=$like_count?> <?=$text_likes?></p>
                                </div>
                                <div class="counts__comments">
                                    <i class="icon icon__comments"></i>
                                    <p><?=$comment_count?> <?=$text_comments?></p>
                                </div>
                            </div>
                            <?php
                            echo !empty($text) ? "<div class=\"text-general\"><p>{$text}</p></div>" : "";
                            ?>
                        </a>
                    </div>
                    <?php
                    $i++;
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}