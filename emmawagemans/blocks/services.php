<div class="block block-services">
    <div class="wrapper">
        <div class="services-in">
            <?php
            global $pattern_variant;
            $type = get_sub_field('services_type');
            $pattern_variant = get_sub_field('pattern_variant');
            if($type == "manual"){
                $services = get_sub_field('services');
            }else{
                $args = array(
                    'taxonomy'   => 'project_services',
                    'hide_empty' => false,
                );
                $services = get_terms($args);
            }
            foreach($services as $post){
                setup_postdata($post);
                get_template_part('loops/service');
            }
            wp_reset_postdata();
            ?>
        </div>
        <div class="clear"></div>
    </div>
</div>