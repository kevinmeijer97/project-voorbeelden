<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <?php
    $favicon = get_custom_src_of_imagefield(get_field('favicon', 'option'), 'favicon');
    if(!empty($favicon)){
        ?>
        <link href="<?php echo $favicon ?>"
              rel="shortcut icon" target="_self">
        <link href="<?php echo $favicon ?>"
              rel="apple-touch-icon-precomposed" target="_self">
        <?php
    }
    ?>
    <title><?php wp_title(); ?></title>
    <?php wp_head(); // DO NOT TOUCH ?>

</head>
<?php
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$safariClass = "";
if(stripos($user_agent, 'Chrome') !== false){
    $safariClass = "";
}elseif(stripos($user_agent, 'Firefox') !== false){
    $safariClass = "";
}elseif(stripos($user_agent, 'Safari') !== false){
    $safariClass = " safari";
}
?>
<body class="loading<?=setCustomGradientColor()?><?=$safariClass?>">

<div class="loader-div gradient">
    <div class="overlay"></div>
    <div class="loader"></div>
</div>
<?php
get_template_part('parts/header');