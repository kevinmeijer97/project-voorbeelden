<?php
$variant = get_sub_field('block_variant');
$image = get_sub_field('image');
$title = get_sub_field('title');
$text = get_sub_field('text');
$has_button = get_sub_field('has_button');
$pattern_variant = get_sub_field('pattern_variant');
ob_start();
if(!empty($image)){
    ?>
    <div class="featured-item-image">
        <img src="<?=get_custom_src_of_imagefield($image, 'small')?>" alt="">
        <div class="underlay gradient">
            <div class="underlay-in" style="background-image: url('<?=get_template_directory_uri()?>/assets/images/pattern-<?=$pattern_variant?>.svg')"></div>
        </div>
    </div>
    <?php
}
$image_part = ob_get_clean();

ob_start();
echo !empty($title) ? "<h2 class=\"title\">{$title}</h2>" : "";
echo !empty($text) ? "<div class=\"text-general\">{$text}</div>" : "";

if($has_button){
    $link = get_sub_field('link');
    if(!empty($link['title'])){
        ?>
        <a href="<?=$link['url']?>"<?=(!empty($link['target']) ? " target=\"{$link['target']}\"" : "")?> class="btn btn-gradient">
            <?=$link['title']?>
            <i class="icon icon__arrow-white"></i>
        </a>
        <?php
    }
}
$text_part = ob_get_clean();
?>
<div class="block block-featured-item<?=$variant == "image_right" ? "-variant-2" : ""?>">
    <div class="wrapper">
        <div class="columns">
            <div class="column column-1">
                <?php
                if($variant == "image_left"){
                    echo $image_part;
                }else{
                    echo $text_part;
                }
                ?>
            </div>
            <div class="column column-2">
                <?php
                if($variant == "image_right"){
                    echo $image_part;
                }else{
                    echo $text_part;
                }
                ?>
            </div>
        </div>
    </div>
</div>