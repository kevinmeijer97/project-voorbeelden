<?php


function disableJQuery(){
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . "/assets/javascripts/jquery.js", false, null, true);
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'disableJQuery', 11);
add_filter('gform_confirmation_anchor', '__return_true');
add_filter('gform_init_scripts_footer', '__return_true');


function add_rewrite_rules($rules){
    $new_rules = [];

    $rules = $new_rules + $rules;

    return $rules;
}

add_filter('rewrite_rules_array', 'add_rewrite_rules');

function refresh_location_permalinks(){
    global $wp_rewrite;
    $wp_rewrite->flush_rules();

}

function init_sessions(){
    if(!session_id()){
        session_start();
        //Set session variables
    }
}

add_action("init", "init_sessions", 1);


add_filter('acf/fields/wysiwyg/toolbars', 'my_toolbars');
function my_toolbars($toolbars){
    $toolbars['Very Simple'] = array();
    $toolbars['Very Simple'][1] = array('bold', 'italic', 'underline', 'link');
    $toolbars['Bold only'] = array();
    $toolbars['Bold only'][1] = array('bold');

    return $toolbars;
}

add_filter('tiny_mce_before_init', 'configure_tinymce');
function configure_tinymce($in){
    $allowed = "b,strong,i,em,a,br";
    $in['paste_preprocess'] = "function(plugin, args){
    // Strip all HTML tags except those we have whitelisted
    var whitelist = '{$allowed}';
    var stripped = jQuery('<div>' + args.content + '</div>');
    var els = stripped.find('*').not(whitelist);
    for (var i = els.length - 1; i >= 0; i--) {
      var e = els[i];
      jQuery(e).replaceWith(e.innerHTML);
    }
    // Strip all class and id attributes
    stripped.find('*').removeAttr('id').removeAttr('class');
    // Return the clean HTML
    args.content = stripped.html();
  }";

    return $in;
}

add_action('load-page.php', 'hide_tinyeditor_wp');

function hide_tinyeditor_wp(){
// Not really necessary, but just in case
    if(!isset($_GET['post'])){
        return;
    }

    $template = get_post_meta($_GET['post'], '_wp_page_template', true);

    if($template_file == 'specific-template.php'){ // edit the template name
        remove_post_type_support('page', 'editor');
    }
}

function getExcerpt($str, $startPos = 0, $maxLength = 100){
    if(strlen($str) > $maxLength){
        $excerpt = substr($str, $startPos, $maxLength - 3);
        $lastSpace = strrpos($excerpt, ' ');
        $excerpt = substr($excerpt, 0, $lastSpace);
        $excerpt .= ' [...]';
    }else{
        $excerpt = $str;
    }

    return $excerpt;
}

add_filter('wpcf7_form_elements', function ($content){
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});


//add_action("pre_get_posts", "changePostQueryForService");

function changePostQueryForService($query){
    if(isset($_GET['filter-services'])){
        if(!is_admin() && $query->is_main_query()){
            $taxquery = array(
                array(
                    'taxonomy' => 'filter-services',
                    'field'    => 'slug',
                    'terms'    => array($_GET['filter-services']),
                    'operator' => 'IN'
                )
            );

            $query->tax_query->queries[] = $taxquery;
            $query->query_vars['tax_query'] = $query->tax_query->queries;
        }
    }

    return $query;
}

function setCustomGradientColor(){
    $gradient = '';
    $has_custom_gradient = get_field('theme_has_custom_gradient', 'option');
    if($has_custom_gradient){
        $custom_gradient = get_field('theme_custom_gradient', 'option');
        $gradient = " " . $custom_gradient;
    }
    return $gradient;
}

add_filter( 'wpseo_breadcrumb_single_link', 'filter_breadcrumb_content', 10, 2 );

function filter_breadcrumb_content( $link_output, $link )
{
    global $wp;
    $current_url = home_url(add_query_arg(array(), $wp->request)) . "/";
    $active = "";
    if($current_url == $link['url']){
        $active = " class=\"active\"";
    }
    $link_output = str_replace("span", "div", $link_output);
    $link_output = "<a href=\"" . ($current_url == $link['url'] ? "javascript:void(0)" : $link['url']) . "\"{$active} target=\"_self\">{$link['text']}</a>";
    return $link_output;
}

function breadcrumb_output_wrapper($output) {
    $output = "div";
    return apply_filters( 'breadcrumb_output_wrapper',  $output);
}

add_filter( 'wpseo_breadcrumb_output_wrapper', 'breadcrumb_output_wrapper', 10, 1 );