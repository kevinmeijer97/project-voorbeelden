<?php
$title = get_sub_field('title');
echo !empty($title) ? "<h4 class=\"text-title\">{$title}</h4>" : "";
