<?php
$background_image = get_sub_field('background_image');
$title = get_sub_field('title');
$background_type = get_sub_field('background_type');
$hide_gradient_on_play = get_sub_field('hide_gradient_on_play');
$has_scrolldown = get_sub_field('has_scrolldown');
$autoplay_video = get_sub_field('autoplay_video');
?>
<div class="block block-featured-media no-padding-bottom no-padding-top"
     style="background-image: url('<?= get_custom_src_of_imagefield($background_image, 'large') ?>')">
    <?php
    if (!get_sub_field('hide_gradient')) {
        ?>
        <div class="gradient<?= $hide_gradient_on_play ? " hide-on-play" : "" ?>"></div>
        <?php
    }

    if ($background_type == "slider") {
        if (have_rows('background_slider')) {
            ?>
            <div class="featured-media-slider">
                <?php
                while (have_rows('background_slider')) {
                    the_row();
                    $image = get_sub_field('image');
                    echo !empty($image) ? "<div class=\"slider-item\" style=\"background-image: url('" . get_custom_src_of_imagefield($image, 'large') . "')\"></div>" : "";
                }
                ?>
            </div>
            <?php
        }
    } elseif ($background_type != "image") {
        $videoUrl = "";
        if ($background_type == "video") {
            $videoUrl = get_sub_field('background_video')['url'];
        } elseif ($background_type == "youtube") {
            $videoUrl = get_sub_field('background_youtube');
        }
        if (!empty($videoUrl)) {
            ?>
            <div class="featured-media-video<?= $autoplay_video ? " has-autoplay" : "" ?>">
                <video
                        id="featured_video"
                        class="video-js vjs-default-skin"
                        muted
                        poster="<?= get_custom_src_of_imagefield($background_image, 'large') ?>"
                        width="1980" height="1200"
                        data-setup='{ "techOrder": ["youtube", "html5"], "sources": [{ "type": "video/<?= $background_type == "video" ? "mp4" : $background_type ?>", "src": "<?= $videoUrl ?>"}] }'
                >
                </video>
            </div>
            <?php
        }
    }
    ?>
    <div class="wrapper">
        <div class="featured-media-in">
            <?php
            echo !empty($title) ? "<div class=\"title-in\"><h1 class=\"title-small\">{$title}<span></span></h1></div>" : "";
            if ($background_type != "image" && $background_type != "slider") {
                ?>
                <div class="featured-media-buttons">
                    <div class="sound-button btn btn-round">
                        <i class="icon icon__mute-white"></i>
                    </div>

                    <div class="play-button btn btn-round">
                        <i class="icon icon__play-white"></i>
                    </div>
                </div>

                <?php
            }
            if ($has_scrolldown) {
                ?>
                <div class="featured-media-scroll">
                    <div class="btn btn-round-large">
                        <i class="icon icon__chevron-down-white"></i>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>