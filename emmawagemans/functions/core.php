<?php

/** DO NOT EDIT!!!!! **/

if(function_exists('add_theme_support')){
    add_theme_support('menus');
    add_theme_support('post-thumbnails');
    add_theme_support('automatic-feed-links');
}

//function acf_google_map_api( $api ){
//    $api['key'] = 'AIzaSyDW2kAV7sOkQ26UOmFv8MTMxMVkaA1VIpw';
//    return $api;
//}

add_filter('acf/fields/google_map/api', 'acf_google_map_api');

function getPostTypeLabels($single, $plural){
    return array(
        'name'               => "{$plural}",
        'singular_name'      => "{$plural}",
        'add_new'            => "Toevoegen",
        'add_new_item'       => "Nieuwe {$single} toevoegen",
        'edit'               => "Wijzig",
        'edit_item'          => "Wijzig {$single}",
        'new_item'           => "Nieuwe {$single}",
        'view'               => "Toon",
        'view_item'          => "Toon {$single}",
        'search_items'       => "Zoeken in {$plural}",
        'not_found'          => "Geen {$plural} gevonden",
        'not_found_in_trash' => "Geen {$plural} gevonden in de vuilnisbak",
    );
}


/*------------------------------------*\
    Functions
\*------------------------------------*/


// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = ''){
    $args['container'] = false;

    return $args;
}


// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist){
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes){
    global $post;
    if(is_home()){
        $key = array_search('blog', $classes);
        if($key > -1){
            unset($classes[$key]);
        }
    }elseif(is_page()){
        $classes[] = sanitize_html_class($post->post_name);
    }elseif(is_singular()){
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style(){
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function itsmeijerwp_pagination(){
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base'    => str_replace($big, '%#%', get_pagenum_link($big)),
        'format'  => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total'   => $wp_query->max_num_pages
    ));
}

// Remove 'text/css' from our enqueued stylesheet
function itsmeijer_style_remove($tag){
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions($html){
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);

    return $html;
}

// Threaded Comments
function enable_threaded_comments(){
    if(!is_admin()){
        if(is_singular() AND comments_open() AND (get_option('thread_comments') == 1)){
            wp_enqueue_script('comment-reply');
        }
    }
}


/*------------------------------------*\
    Actions + Filters + ShortCodes
    Source: https://github.com/html5blank/html5blank/blob/master/src/functions.php
\*------------------------------------*/

// Add Actions
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
//add_action('init', 'register_itsmeijer_menu'); // Add Brick Zero Menu
add_action('init', 'create_post_type_itsmeijer'); // Add Custom Post Types
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'itsmeijerwp_pagination'); // Add our itsmeijer Pagination
add_action('p2p_init', 'post_connections');

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);


// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('style_loader_tag', 'itsmeijer_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

/*
 * This is an example of filtering menu parameters
 */

function get_custom_src_of_imagefield($field, $size = false){
    if(!isset($size)){
        $size = 'large';
    }

    if(isset($field['sized'][$size])){
        return $field['sized'][$size];
    }else{
        $image_src = wp_get_attachment_image_src($field['id'], $size);

        return $image_src[0];
    }
}


function get_image($field, $size = false){
    if(!isset($size)){
        $size = 'large';
    }
    $image_src = wp_get_attachment_image_src($field['id'], $size);
    $alt = get_post_meta($field['id'], '_wp_attachment_image_alt', true);

    return '<img src="' . $image_src[0] . '" alt="' . $alt . '" />';
}

function the_image($field, $size = false){
    if(!isset($size)){
        $size = 'large';
    }
    $image_src = wp_get_attachment_image_src($field['id'], $size);
    $alt = get_post_meta($field['id'], '_wp_attachment_image_alt', true);
    echo '<img src="' . $image_src[0] . '" alt="' . $alt . '" />';
}

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

if(function_exists('acf_add_options_page')){

    acf_add_options_page(array(
        'page_title' => 'Thema',
        'menu_title' => 'Thema',
        'menu_slug'  => 'theme_settings',
        'capability' => 'edit_posts',
        'redirect'   => true,
        'position'   => '3'
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Thema',
        'menu_title'  => 'Thema',
        'parent_slug' => 'theme_settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Instellingen',
        'menu_title'  => 'Instellingen',
        'parent_slug' => 'theme_settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Teksten',
        'menu_title'  => 'Teksten',
        'parent_slug' => 'theme_settings',
    ));


}


// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support(){
    $post_types = get_post_types();
    foreach($post_types as $post_type){
        if(post_type_supports($post_type, 'comments')){
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}

add_action('admin_init', 'df_disable_comments_post_types_support');

function remove_menus(){

    remove_menu_page('edit.php');                   //Posts
    remove_menu_page('edit-comments.php');          //Comments

}

add_action('admin_menu', 'remove_menus');

function wpse_hide_cat_descr(){
    ?>

    <style type="text/css">
        .term-description-wrap {
            display: none;
        }
    </style>

    <?php
}

add_action('admin_head-term.php', 'wpse_hide_cat_descr');
