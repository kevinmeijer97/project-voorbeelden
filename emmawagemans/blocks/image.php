<?php
$image = get_sub_field('image');
if(!empty($image)){
    ?>
    <div class="block block-image">
        <div class="wrapper">
            <div class="block-image-in" style="background-image: url('<?=get_custom_src_of_imagefield($image, 'large')?>')"></div>
        </div>
    </div>
    <?php
}