<?php
get_header();

global $post;

$prev_post = get_previous_post();
$next_post = get_next_post();
if($prev_post->ID == null){
    $prev_args = array(
        'post_type'      => 'project',
        'posts_per_page' => 1,
        'orderby'        => 'date',
        'order'          => 'DESC',
        'post__not_in'   => array($post->ID)
    );
    $prev_post = get_posts($prev_args);
    $prev_post = $prev_post[0];
}
if($next_post->ID == null){
    $next_args = array(
        'post_type'      => 'project',
        'posts_per_page' => 1,
        'orderby'        => 'date',
        'order'          => 'ASC',
        'post__not_in'   => array($post->ID)
    );
    $next_post = get_posts($next_args);
    $next_post = $next_post[0];
}

$subtitle = get_field('detail_subtitle');
$image = get_field('background_image');
?>
    <div class="block block-title single-project" style="background-image: url('<?=get_custom_src_of_imagefield($image, 'large')?>');">
        <div class="wrapper wrapper__small">
            <div class="gradient"></div>
            <div class="title-small"><?=$post->post_title?></div>
            <?php
            echo !empty($subtitle) ? "<div class=\"sub-title\">{$subtitle}</div>" : "";
            if(function_exists('yoast_breadcrumb')){
                $breadcrumbs = yoast_breadcrumb('<div class="breadcrumbs">', '</div>');
                echo $breadcrumbs;
            }
            ?>
        </div>
    </div>
    <div class="block block-portfolio-detail">
        <div class="wrapper wrapper__small">
            <div class="portfolio-top">
                <div class="portfolio-specifics">
                    <div class="portfolio-specifics-in">
                        <?php
                        $specs = get_field('specifications_group');
                        $specifications = $specs['specifications'];
                        if(!empty($specifications)){
                            foreach($specifications as $specification){
                                $icon = $specification['icon'];
                                if($icon == "__agenda"){
                                    $text = $specification['date'];
                                }else{
                                    $text = $specification['text'];
                                }
                                $tooltip = $specification['tooltip'];
                                if(!empty($text)){
                                    ?>
                                    <div class="portfolio-specifics__item">
                                        <div class="portfolio-specifics-content">
                                            <i class="<?=$icon == "-diamond" || $icon == "-sewing-machine" ? "alt-" : ""?>icon icon<?=$icon?>"></i>
                                            <p class="portfolio-specifics__text"><?=$text?></p>
                                        </div>
                                        <div class="portfolio-specifics-tooltip">
                                            <div class="text-general">
                                                <p><?=$tooltip?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <?php
            if(have_rows('detail_content')){
                while(have_rows('detail_content')){
                    the_row();
                    $block = str_replace(['content_', '_'], ['', '-'], get_row_layout());
                    get_template_part('blocks/content/' . $block);
                }
            }
            ?>
            <div class="portfolio-nav">
                <?php
                //next is actually previous and previous is next because of sorting order on the archive.
                if(!empty($next_post)){
                    ?>
                    <a href="<?=get_permalink($next_post)?>" class="btn btn-gradient portfolio-nav-prev">
                        <i class="icon icon__arrow-white"></i>
                        <?=get_field('text_previous', 'option')?>
                    </a>
                    <?php
                }
                if(!empty($prev_post)){
                    ?>
                    <a href="<?=get_permalink($prev_post)?>" class="btn btn-gradient portfolio-nav-next">
                        <i class="icon icon__arrow-white"></i>
                        <?=get_field('text_next', 'option')?>
                    </a>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

<?php
get_footer();