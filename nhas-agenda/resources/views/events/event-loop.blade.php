@isset($event)

    <div class="card mb-25">
        @php
            $start_date = new Date($event->start_date);
            $dateDifference = Date::parse($event->start_date)->diffForHumans();
        @endphp
        <div class="card-header">{{$event->location}} - {{$start_date->format('l j F Y')}}
            <div class="days-left">
                <strong>Nog {{$dateDifference}}</strong>
            </div>
        </div>

        <div class="card-body">
            <p>Voor dit evenement zijn <strong>{{count($event->users)}}</strong> persoon/personen ingeschreven.</p>
            <p><strong>Ingeschreven:</strong> {{getEventUsersData($event->users)}}</p>
        </div>

        @if(Auth::user() && Auth::user()->email_verified_at !== null)
            <div class="card-footer">
                <div class="button button-blue">
                    <a href="{{url('event/' . $event->id . '/')}}" target="_self">Bekijk evenement</a>
                </div>
            </div>
        @endif
    </div>
@endisset
