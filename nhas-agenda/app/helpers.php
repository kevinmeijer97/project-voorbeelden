<?php
function getDateDifference($date, $dateToCompare)
{


    $date = strtotime($date);
    $dateToCompare = strtotime($dateToCompare);

    $diff = abs($dateToCompare - $date);

    $years = floor($diff / (365 * 60 * 60 * 24));

    $months = floor(($diff - $years * 365 * 60 * 60 * 24)
        / (30 * 60 * 60 * 24));

    $days = floor(($diff - $years * 365 * 60 * 60 * 24 -
            $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

    $hours = floor(($diff - $years * 365 * 60 * 60 * 24
            - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24)
        / (60 * 60));

    if ($years > 1) {
        $return = $years . ' jaar';
    } elseif ($months > 1) {
        $return = $months . ' maanden';
    } elseif ($days > 1) {
        $return = $days . ' dagen';
    } elseif ($hours > 1) {
        $return = $hours . ' uur';
    }


    return $return;
}


function getEventUsersData($eventUsers)
{

    $return = '';
    if (!empty($eventUsers)) {
        $i = 0;
        foreach ($eventUsers as $eventUser) {
            if ($i == (count($eventUsers) - 2)) {
                $return .= $eventUser->name . ' en ';
            } elseif ($i == (count($eventUsers) - 1)) {
                $return .= $eventUser->name . '.';
            } else {
                $return .= $eventUser->name . ', ';
            }
            $i++;
        }
    }

    return $return;
}


function userSignedUp($user, $eventId) {

}
