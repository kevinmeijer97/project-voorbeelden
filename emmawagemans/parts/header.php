<?php
global $wp;
$current_url = home_url(add_query_arg(array(), $wp->request)) . "/";
?>
<header>
    <div class="wrapper">
        <div class="header-in">
            <div class="mobile-toggle">
                <div class="mobile-toggle-in">
                    <div class="header-mobile-hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="header-nav">
                <div class="header-nav-in columns">
                    <?php
                    $left_nav = get_field('header_nav_left', 'option');
                    $right_nav = get_field('header_nav_right', 'option');
                    if(!empty($left_nav)){
                        ?>
                        <div class="column column-1">
                            <?php
                            foreach($left_nav as $item){
                                $link = $item['link'];
                                if ($current_url == $link['url']) {
                                    $active = " class=\"active\"";

                                } else {
                                    $active = "";
                                }
                                echo !empty($link['title']) ? "<a href=\"{$link['url']}\"{$active} " . ($link['target'] == "_blank" ? " target=\"{$link['target']}\"" : "") . ">{$link['title']}</a>" : "";
                            }
                            ?>
                        </div>
                        <?php
                    }
                    if(!empty($right_nav)){
                        ?>
                        <div class="column column-2">
                            <?php
                            foreach($right_nav as $item){
                                $link = $item['link'];
                                if ($current_url == $link['url']) {
                                    $active = " class=\"active\"";

                                } else {
                                    $active = "";
                                }
                                echo !empty($link['title']) ? "<a href=\"{$link['url']}\"{$active} " . ($link['target'] == "_blank" ? " target=\"{$link['target']}\"" : "") . ">{$link['title']}</a>" : "";
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="footer-socials">
                        <?php
                        $twitter = get_field('social_twitter', 'option');
                        $linkedin = get_field('social_linkedin', 'option');
                        $instagram = get_field('social_instagram', 'option');
                        $facebook = get_field('social_facebook', 'option');
                        $youtube = get_field('social_youtube', 'option');

                        echo !empty($twitter) ? "<a href=\"{$twitter}\" target=\"_blank\"><i class=\"icon icon__twitter-white\"></i></a>" : "";
                        echo !empty($linkedin) ? "<a href=\"{$linkedin}\" target=\"_blank\"><i class=\"icon icon__linkedin-white\"></i></a>" : "";
                        echo !empty($instagram) ? "<a href=\"{$instagram}\" target=\"_blank\"><i class=\"icon icon__instagram-white\"></i></a>" : "";
                        echo !empty($facebook) ? "<a href=\"{$facebook}\" target=\"_blank\"><i class=\"icon icon__facebook-white\"></i></a>" : "";
                        echo !empty($youtube) ? "<a href=\"{$youtube}\" target=\"_blank\"><i class=\"icon icon__youtube-white\"></i></a>" : "";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $logo_type = get_field('logo_type', 'option');
        if($logo_type == "text") {
            $logo = get_field('logo_text', 'option');
        }else {
            $logo = "<img src=\"" . (get_custom_src_of_imagefield(get_field('logo', 'option'), 'small')) . "\" alt=\"\">";
        }
        if(!empty($logo)){
            ?>
            <a href="<?=home_url()?>" class="header-title title-small">
                <?=$logo?>
            </a>
            <?php
        }
        ?>
    </div>
</header>