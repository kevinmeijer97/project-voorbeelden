<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationController extends Controller
{
    public function createPost(Request $request, $eventId)
    {
        $messageData = $request->validate([
            'message' => 'required|max:255',
        ]);

        $event = Event::find($eventId);
        $userId = Auth::User()->id;
        $message = $messageData['message'];

        $conversation = new Conversation();
        $conversation->user_id = $userId;
        $conversation->event_id = $event->id;
        $conversation->message = $message;
        $conversation->save();


        return redirect('event/' . $event->id);
    }
}
