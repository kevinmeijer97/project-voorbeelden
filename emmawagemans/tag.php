<?php
get_header();
global $tag;

$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'post_tag' => $tag,
    'posts_per_page' => -1,
);
$posts = get_posts($args);

if (!empty($posts)) {
    ?>
    <section id="three" class="wrapper style2">
        <div class="inner">
            <h2 class="align-center"><?= single_tag_title() ?></h2>
            <div class="grid-style">
                <?php
                foreach ($posts as $post) {
                    setup_postdata($post);
                    get_template_part('loops/post');
                }
                ?>
            </div>
        </div>
    </section>
    <?php
}
wp_reset_postdata();
get_template_part('parts/recent-tags');
get_footer();