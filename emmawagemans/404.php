<?php
get_header();

$image = get_field('404_page_image', 'option');
if(!empty($image)){
    $title = get_field('404_page_title', 'option');
    $subtitle = get_field('404_page_subtitle', 'option');
    $button = get_field('404_page_button', 'option');
    ?>
    <div class="block block-featured-item">
        <div class="wrapper">
            <div class="columns">
                <div class="column column-1">
                    <div class="featured-item-image">
                        <img src="<?=get_custom_src_of_imagefield($image, 'large')?>" alt="">
                        <div class="underlay gradient">
                            <div class="underlay-in" style="background-image: url('http://emmawagemans.itsmeijer.nl/wp-content/themes/itsmeijer/assets/images/pattern-waves.svg')"></div>
                        </div>
                    </div>
                </div>
                <div class="column column-2">
                    <?php
                    echo !empty($title) ? "<h2 class=\"title\">{$title}</h2>" : "";
                    echo !empty($subtitle) ? "<div class=\"text-general\"><p>{$subtitle}</p></div>" : "";
                    ?>
                    <a href="<?=$button['url']?>"<?=(!empty($button['target']) ? " target=\"{$button['target']}\"" : "")?> class="btn btn-gradient">
                        <?=$button['title']?> <i class="icon icon__arrow-white"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php
}

get_footer();
