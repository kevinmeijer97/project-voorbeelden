<?php
if(have_rows('blocks')){
    while(have_rows('blocks')){
        the_row();
        $block = str_replace(['block_', '_'], ['', '-'], get_row_layout());
        get_template_part('blocks/' . $block);
    }
}