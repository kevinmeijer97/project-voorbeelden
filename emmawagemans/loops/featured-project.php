<?php
global $post;

$featured_image = get_field('featured_image');
$subtitle = get_field('project_subtitle');
$date = get_field('project_date');
$newDate = explode('/', $date);
if(!empty($featured_image)){
    ?>
    <div class="featured-image">
        <a href='<?=get_permalink()?>' class="featured-image-in" style="background-image: url('<?=get_custom_src_of_imagefield($featured_image, 'featured_image')?>')">
            <div class="overlay"></div>
            <div class="shadow"></div>
            <?php
            if(!empty($newDate)){
                $day = $newDate[0];
                $month = $newDate[1];
                $year = $newDate[2];
                ?>
                <div class="featured-image-date"><?=$day?> <?=$month?><span><?=$year?></span></div>
                <?php
            }
            ?>
            <h3 class="featured-image-title"><?=$post->post_title?><?=!empty($subtitle) ? "<span>{$subtitle}</span>" : ""?></h3>
        </a>
    </div>
    <?php
}