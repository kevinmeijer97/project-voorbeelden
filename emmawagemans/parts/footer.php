<footer>
    <div class="gradient gradient-diagonal">
        <div class="underlay-in">
            <img src="<?=get_template_directory_uri()?>/assets/images/pattern-hexagon.svg" alt="">
        </div>
    </div>
    <div class="overlay"></div>
    <div class="wrapper">
        <div class="footer-form">
            <?php
            $contact_title = get_field('footer_contact_title', 'option');

            echo !empty($contact_title) ? "<h2 class=\"title\">{$contact_title}</h2>" : "";
            ?>
            <div class="footer-form-in">
                <div class="footer-form-left" data-form-gradient="">
                    <h4 class="footer-title"><?=get_field('text_send_a_message', 'option')?></h4>
                    <?php
                    echo do_shortcode('[contact-form-7 id="121" title="Contact"]');
                    ?>

                </div>
                <?php
                $contact_text = get_field('footer_contact_text', 'option');
                ?>
                <div class="footer-form-right">
                    <h4 class="footer-title"><?=get_field('text_contact_us_directly', 'option')?></h4>

                    <div class="footer-form-social">
                        <?php
                        $email = get_field('theme_email ', 'option');
                        if(!empty($email)){
                            ?>
                            <div class="footer-form-social-left gradient">
                                <i class="icon icon__envelope-white"></i>
                            </div>
                            <div class="footer-form-social-right">
                                <a class="text-general" href="mailto:<?=$email?>"><?=$email?></a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    echo !empty($contact_text) ? "<div class=\"text-general\"><p>{$contact_text}</p></div>" : "";
                    ?>
                </div>
            </div>
        </div>
        <?php
        $footer_rows = get_field('footer_rows', 'option');
        ?>
        <div class="footer-in">
            <?php
            if(!empty($footer_rows)){
                $i = 1;
                foreach($footer_rows as $row){
                    $row_title = $row['title'];
                    $row_type = $row['row_type'];
                    ?>
                    <div class="footer-column column-<?=$i?>">
                        <?php
                        echo !empty($row_title) ? "<h4 class=\"footer-title\">{$row_title}</h4>" : "";
                        if($row_type == "text"){
                            $row_text = $row['text'];
                            echo !empty($row_text) ? "<div class=\"text-general\">{$row_text}</div>" : "";
                        }else{
                            $row_links = $row['links'];
                            if(!empty($row_links)){
                                echo "<ul class=\"text-general\">";
                                foreach($row_links as $row_link){
                                    $link = $row_link['link'];
                                    echo !empty($link['title']) ? "<li><a href=\"{$link['url']}\" " . (!empty($link['target']) ? " target=\"{$link['target']}\"" : "") . ">{$link['title']}</a></li>" : "";
                                }
                                echo "</ul>";
                            }
                        }
                        ?>
                    </div>
                    <?php
                    $i++;
                }
            }
            ?>
            <div class="footer-column column-3 footer-socials">
                <h4 class="footer-title"><?=get_field('text_social', 'option')?></h4>
                <?php
                $twitter = get_field('social_twitter', 'option');
                $linkedin = get_field('social_linkedin', 'option');
                $instagram = get_field('social_instagram', 'option');
                $facebook = get_field('social_facebook', 'option');
                $youtube = get_field('social_youtube', 'option');

                echo !empty($twitter) ? "<a href=\"{$twitter}\" target=\"_blank\"><i class=\"icon icon__twitter-white\"></i></a>" : "";
                echo !empty($linkedin) ? "<a href=\"{$linkedin}\" target=\"_blank\"><i class=\"icon icon__linkedin-white\"></i></a>" : "";
                echo !empty($instagram) ? "<a href=\"{$instagram}\" target=\"_blank\"><i class=\"icon icon__instagram-white\"></i></a>" : "";
                echo !empty($facebook) ? "<a href=\"{$facebook}\" target=\"_blank\"><i class=\"icon icon__facebook-white\"></i></a>" : "";
                echo !empty($youtube) ? "<a href=\"{$youtube}\" target=\"_blank\"><i class=\"icon icon__youtube-white\"></i></a>" : "";
                ?>
            </div>
        </div>
    </div>
</footer>