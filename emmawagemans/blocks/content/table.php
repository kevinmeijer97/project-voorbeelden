<?php
$table = get_sub_field('table');
if(!empty($table)){
    ?>
    <table>
        <?php
        foreach($table as $row){
            $left_text = $row['left_text'];
            $right_text = $row['right_text'];
            ?>
            <tr>
                <?php
                echo !empty($left_text) ? "<td>{$left_text}</td>" : "";
                echo !empty($right_text) ? "<td>{$right_text}</td>" : "";
                ?>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
}