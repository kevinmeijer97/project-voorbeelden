<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Wij hebben deze gegevens niet kunnen vinden.',
    'throttle' => 'Te veel inlog pogingen. Probeer het op een ander moment nog een keer.',

];
