<?php
get_header();
$title = get_field('projects_archive_title', 'option');
$image = get_field('projects_archive_image', 'option');
$pattern = get_field('projects_archive_pattern', 'option');
if(isset($_GET['project-filter'])){
    $selected_terms = $_GET['project-filter'];
}else{
    $selected_terms = '';
}
?>
    <div class="block block-title" style="background-image: url('<?=get_custom_src_of_imagefield($image, 'large')?>');">
        <div class="wrapper">
            <div class="gradient"></div>
            <?php
            if(function_exists('yoast_breadcrumb')){
                $breadcrumbs = yoast_breadcrumb('<div class="breadcrumbs">', '</div>');
                echo $breadcrumbs;
            }

            echo !empty($title) ? "<div class=\"title-in\"><div class=\"title-small\">{$title}</div></div>" : "";
            ?>
        </div>
    </div>
<?php
if(have_posts()){
    ?>
    <div class="block block-portfolio-archive">
        <div class="wrapper">
            <div class="portfolio-archive-top">

                <div class="portfolio-filters">
                    <div class="filters-content">
                        <?php
                        $args = array(
                            'taxonomy'   => 'project_services',
                            'hide_empty' => false,
                        );
                        $terms = get_terms($args);

                        if(!empty($terms)){
                            ?>
                            <div class="filter-select">
                                <div class="filter-placeholder"><?=get_field('text_select_an_option', 'option')?></div>
                                <div class="filter-arrow">
                                    <i class="icon icon__arrow-down-white"></i>
                                </div>
                            </div>
                            <div class="filter-dropdown">
                                <?php
                                foreach($terms as $term){

                                    ?>
                                    <div class="filter-item" data-filter-item="<?=$term->slug?>"><?=$term->name?> <span class="gradient"></span></div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="active-filters">
                    <?php
                    foreach($terms as $term){
                        $active = "";
                        if($selected_terms == $term->slug) {
                            $active = " filter-active";
                        }
                        ?>
                        <div class="active-filter<?=$active?>" <?=$selected_terms != $term->slug ? "style=\"display: none;\"" : ""?> data-filter-active="<?=$term->slug?>"><?=$term->name?> <i class="icon icon__cross"></i></div>
                        <?php
                    }
                    ?>
                </div>
                <div class="clear"></div>
            </div>

            <div class="portfolio-archive-in">
                <div class="underlay gradient">
                    <div class="underlay-in" style="background-image: url('<?=get_template_directory_uri()?>/assets/images/pattern-<?=$pattern?>.svg')"></div>
                </div>
                <?php
                while(have_posts()){
                    the_post();
                    get_template_part('loops/project');
                }
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
    <?php
}
get_footer();