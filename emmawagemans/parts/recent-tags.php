<section id="three" class="wrapper style2">
    <div class="inner">
        <h2 class="align-center"><?= get_field('text_tags', 'option') ?></h2>
        <div class="align-center">
            <?php
            $tags = get_tags(array(
                'hide_empty' => false
            ));
            foreach ($tags as $tag) {
                $tag_link = get_tag_link($tag);
                echo "<a href=\"{$tag_link}\">{$tag->name}</a>, ";
            }
            ?>
        </div>
    </div>
</section>