<?php
$images = get_sub_field('images');
if(!empty($images)){
    ?>
    <div class="portfolio-images">
        <div class="portfolio-images-in">
            <?php
            foreach($images as $item){
                $image = $item['image'];
                $caption = $item['caption'];
                $image_small = get_custom_src_of_imagefield($image, 'small');
                $image_big = get_custom_src_of_imagefield($image, 'large');
                if(!empty($image_small) && !empty($image_big)){
                    ?>
                    <a href="<?=$image_big?>" class="portfolio-image fancybox-thumb" title="<?=$caption?>" rel="portfolio-lightbox">
                        <img src="<?=$image_small?>" alt="">
                    </a>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <?php
}