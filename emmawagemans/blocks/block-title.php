<?php
$image = get_sub_field('image');
if(!empty($image)){
    $title = get_sub_field('title');
    ?>
    <div class="block block-title" style="background-image: url('<?=get_custom_src_of_imagefield($image, 'large')?>');">
        <div class="wrapper">
            <div class="gradient"></div>
            <?php
            echo !empty($title) ? "<div class=\"title-small\">{$title}</div>" : "";
            ?>
        </div>
    </div>
    <?php
}