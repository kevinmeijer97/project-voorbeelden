@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card mb-50">
                    @php
                        $start_date = new Date($event->start_date);
                        $dateDifference = Date::parse($event->start_date)->diffForHumans();
                    @endphp
                    <div class="card-header">{{$event->location}} - {{$start_date->format('l j F Y')}}
                        <div class="days-left">
                            <strong>Nog {{$dateDifference}}</strong>
                        </div>
                    </div>

                    <div class="card-body">
                        <p>Voor dit evenement zijn <strong>{{count($event->users)}}</strong> persoon/personen
                            ingeschreven.</p>
                        <p><strong>Ingeschreven:</strong> {{getEventUsersData($event->users)}}</p>
                    </div>
                    @if(Auth::user() && Auth::user()->email_verified_at !== null)
                        <div class="card-footer">

                            @if(Auth::User()->hasEvent($event->id))
                                <div class="button button-red">
                                    <a href="{{url('event/' . $event->id . '/signoff')}}"
                                       target="_self">{{__('custom.signoff')}}</a>
                                </div>
                            @else
                                <div class="button button-blue">
                                    <a href="{{url('event/' . $event->id . '/signup')}}" target="_self">{{__('custom.signup')}}</a>
                                </div>
                            @endif
                        </div>
                    @endif
                </div>

                <div class="card mb-50">
                    <div class="card-header">{{__('custom.conversation')}}</div>

                    <div class="card-body">
                        @isset($conversations)
                            @if(!empty($conversations))
                                <div class="conversation">
                                    @foreach($conversations as $conversation)
                                        <div
                                            class="conversation-message{{Auth::user() && $conversation->user->id == Auth::User()->id ? " to-right" : ""}}">
                                            <div class="conversation-message-container">
                                                <div class="conversation-message-in">
                                                    <div class="conversation-message-top">
                                                        {{$conversation->user->name}}
                                                        <span>{{date('d-m-Y H:i', strtotime($conversation->created_at))}}</span>
                                                    </div>
                                                    <div class="conversation-message-bottom">
                                                        <p>{{$conversation->message}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="clear"></div>
                                </div>
                            @endif
                        @endisset

                        @if(Auth::user() && Auth::user()->email_verified_at !== null)
                            <div class="conversation-form">

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif

                                {{ Form::open(array('url' => '/event/' . $event->id . '/conversation/create/')) }}
                                <div class="form-row">
                                    <div class="input-row">
                                        {{ Form::textarea('message', null, ['cols' => 100, 'placeholder' => 'Bericht'])}}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="input-row">
                                        <div class="button button-blue">
                                            <button type="submit">{{__('custom.send')}}</button>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
