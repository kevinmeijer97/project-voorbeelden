<?php
global $post, $pattern_variant;
$title = $post->name;
$description = get_field('description', $post);
$image = get_field('image', $post);

?>
<a href="<?=get_post_type_archive_link('project')?>?project-filter=<?=$post->slug?>" target="_self" class="service">
    <div class="service-in">
        <?php
        echo !empty($image) ? "<div class=\"service-top\"><img src=\"" . (get_custom_src_of_imagefield($image, 'services')) . "\" alt=\"\"></div>" : "";
        ?>
        <div class="service-content">
            <?php
            echo !empty($title) ? "<div class=\"title\">{$title}</div>" : "";
            echo !empty($description) ? "<div class=\"text-general\">{$description}</div>" : "";
            ?>
        </div>

        <div class="underlay gradient">
            <div class="underlay-in" style="background-image: url('<?=get_template_directory_uri()?>/assets/images/pattern-<?=$pattern_variant?>.svg')"></div>
        </div>
    </div>
</a>