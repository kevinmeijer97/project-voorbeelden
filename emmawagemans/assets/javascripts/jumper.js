/**
 * Plugin Name: jQuery Essentials Jumper
 * Version: 1.6
 * Date: 27-11-2017
 *
 * Usage:
 *
 *
 * <div data-jumper-target="jumperId"></div>
 * <a href="jumpto:jumperId">To Jumper Target</a>
 *
 * <div data-jumper-id="jumperId">Jumper Content</div>
 *
 jQuery(scrollContainer).esJumper({
		offsetContainer: false,
	    offsetTop: 0,
	    perfectScroll: false,
        blockSelector: '.block'
	 });
 *
 **/


(function (jQuery) {

    jQuery.fn.esJumper = function (options) {

        var scrollContainer = jQuery(this);
        var settings = jQuery.extend({
            offsetContainer: false,
            offsetTop: 0,
            perfectScroll: false,
            blockSelector: '.block'
        }, options);


        jQuery('[data-jumper-target]').on('click', function () {
            var jumperId = jQuery(this).attr('data-jumper-target');
            if(jumperId == 'nextBlock'){
                var currentBlock = jQuery(this).parents(settings.blockSelector);
                var nextBlock = currentBlock.next(settings.blockSelector);
                if (nextBlock.length) {
                    jumpToPart(nextBlock);
                }
                return false;
            } else {
                jumpTo(jumperId);
            }
            return false;
        });

        jQuery('body').on('click', 'a[href^="jumpto"]', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var jumpLink = jQuery(this).attr('href');
            var jumperId = jumpLink.substring(7);
            if (jumperId.length) {
                jumpTo(jumperId);
            }
        });

        function jumpToPart(toPart){
            var extraOffset = settings.offsetTop;
            if (settings.offsetContainer) {
                extraOffset += jQuery(settings.offsetContainer).outerHeight();
            }

            if (settings.perfectScroll) {
                var toPartOffset = toPart.position().top + scrollContainer.scrollTop() - extraOffset;
            } else {
                var toPartOffset = toPart.offset().top - extraOffset;
            }


            scrollToTimeout = setTimeout(function () {
                scrollContainer.animate({scrollTop: toPartOffset}, 750, "easeInOutExpo", function () {
                    if (settings.perfectScroll) {
                        scrollContainer.trigger('ps-scroll-y');
                    }
                });
            }, 300);
        }

        function jumpTo(jumperId) {
            var toPart = jQuery('[data-jumper-id="' + jumperId + '"]');
            // CHECK IF IS NEXT

            if (toPart.length) {
                jumpToPart(toPart);
                setActiveJumperItem(jumperId);
            }
        }

        function renderJumperItems() {

            var extraOffset = settings.offsetTop;
            if (settings.offsetContainer) {
                extraOffset += jQuery(settings.offsetContainer).outerHeight();
            }
            var scrollPosition = scrollContainer.scrollTop();
            var scrollContainerHeight = scrollContainer[0].scrollHeight;

            if (scrollContainer.scrollTop() + scrollContainer.height() >= scrollContainerHeight) {
                var lastHeaderitem = jQuery('[data-jumper-id]').last();
                setActiveJumperItem(lastHeaderitem.attr('data-jumper-id'));
            } else {
                jQuery('[data-jumper-id]').each(function () {
                    var jumperId = jQuery(this).attr('data-jumper-id');
                    var toPart = jQuery('[data-jumper-id="' + jumperId + '"]');
                    if (toPart.length) {

                        if (settings.perfectScroll) {
                            var toPartOffset = toPart.position().top + scrollContainer.scrollTop() - extraOffset;
                        } else {
                            var toPartOffset = toPart.offset().top - extraOffset;
                        }
                        if (toPartOffset <= scrollPosition) {
                            setActiveJumperItem(jumperId);
                        }
                    }
                });
            }
        }

        function setActiveJumperItem(jumperId) {
            jQuery('[data-jumper-target].active').removeClass('active');
            jQuery('[data-jumper-target="' + jumperId + '"]').addClass('active');
        }

        if (settings.perfectScroll) {
            scrollContainer.on('ps-scroll-y', function () {
                renderJumperItems();
            });
        } else {
            scrollContainer.on('scroll', function () {
                renderJumperItems();
            });
        }

        jQuery(window).load(function () {
            renderJumperItems();
            if (window.location.hash) {
                var jumperId = window.location.hash.replace("#", "");
                jumpTo(jumperId);
            }
        });

    };

}(jQuery));