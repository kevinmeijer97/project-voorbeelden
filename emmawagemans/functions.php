<?php

/****************** FUNCTIONS ******************/
require_once('functions/core.php'); // NEVER EDIT!!

/************** FUNCTIONS TO EDIT **************/
require_once('functions/custom.php');
require_once('functions/thumbnails.php');
require_once('functions/post-types.php');
require_once('functions/post-connections.php');
require_once('functions/fields.php');