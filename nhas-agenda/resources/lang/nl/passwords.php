<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Je wachtwoord is gewijzigd!',
    'sent' => 'Er is een email verstuurd met een wachtwoord wijzigings link.',
    'throttled' => 'Wacht even en probeer het daarna opnieuw.',
    'token' => 'Deze wachtwoord reset token is niet geldig.',
    'user' => "Wij kunnen geen gebruiker vinden met dat e-mailadres.",

];
