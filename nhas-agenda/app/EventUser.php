<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventUser extends Model
{
    public function getEvent()
    {
        return $this->belongsTo('App\Event');
    }

    public function getUser()
    {
        return $this->belongsTo('App\User');
    }
}
