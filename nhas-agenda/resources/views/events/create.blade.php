@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                {{ Form::open(array('url' => '/event/create/')) }}

                <div class="card mb-50">
                    <div class="card-header">{{__('custom.create_event')}}</div>

                    <div class="card-body">

                        <div class="form-group form-row">
                            <div class="input-row">
                                {{Form::label('event_location', __('custom.location'), ['class' => 'col-form-label'])}}
                                {{Form::text('event_location', null, ['class' => 'form-control'])}}
                                @error('event_location')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group form-row">
                            <div class="input-row">
                                {{Form::label('event_start_date', __('custom.start_date'))}}
                                {{Form::date('event_start_date', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
                            </div>
                        </div>

                        <div class="form-group form-row">
                            <div class="input-row">
                                {{Form::label('event_end_date', __('custom.end_date'))}}
                                {{Form::date('event_end_date', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="button button-blue">
                            <button type="submit">{{__('custom.create_event')}}</button>
                        </div>
                    </div>
                </div>

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
