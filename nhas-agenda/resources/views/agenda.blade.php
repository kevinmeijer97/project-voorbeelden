@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-50">
                    <div class="card-header">{{__('custom.agenda')}}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @isset($events)
                            <div class="alert alert-info" role="alert">
                                Dit is de agenda. Hier kan je zien wie er voor welke evenementen zijn aangemeld, ook kan
                                je je zelf voor een evenement aanmelden of een nieuwe aanmaken.
                            </div>
                        @else
                            <div class="alert alert-info" role="alert">
                                Dit is de agenda. Er zijn op dit moment nog geen evenementen geplaatst.
                            </div>
                        @endisset
                    </div>
                    @if(Auth::user() && Auth::user()->email_verified_at !== null)
                        <div class="card-footer">
                            <div class="button button-green">
                                <a href="{{url('event/create')}}" target="_self">{{__('custom.create_event')}}</a>
                            </div>
                        </div>
                    @endif
                </div>

                @isset($events)
                    @foreach($events as $event)
                        @include('events.event-loop', ['event' => $event])
                    @endforeach
                @endisset
            </div>
        </div>
    </div>
@endsection
