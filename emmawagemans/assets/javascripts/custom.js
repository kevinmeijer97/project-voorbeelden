$(window).on('load', function () {
    addGradientToForm();
    setHeaderColor();
    removeVideoOnSize();
    initItemActive();
    imageSlider();
});

$(window).on('resize', function () {
    removeVideoOnSize();
});

jQuery(document).ready(function () {

    jQuery(".fancybox-thumb").fancybox({
        padding: 0,
        helpers: {
            title: {
                type: 'outside'
            },
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });
});

jQuery('.mobile-toggle-in').on('click', function () {
    $('body').toggleClass('mobile-menu-open');
});

$(window).on("scroll", function () {
    setHeaderColor();
});

jQuery('.featured-image').on('mouseenter', function () {
    var parent = jQuery(this).parent();
    parent.find('.featured-image').addClass('inactive');
    jQuery(this).addClass('active');
});

jQuery('.featured-image').on('mouseleave', function () {
    jQuery(this).removeClass('active');
});

jQuery('.featured-images').on('mouseleave', function () {
    jQuery(this).find('.featured-image').removeClass('inactive');
});

jQuery('.input-row input, .input-row textarea').on('focus', function () {
    var input = jQuery(this);
    input.parent().find('label').addClass('focused');
});

jQuery('.input-row input, .input-row textarea').on('blur', function () {
    var input = jQuery(this);
    if (input.val() == '') {
        input.parent().find('label').removeClass('focused');
    }
});

jQuery('.featured-media-scroll').on('click', function () {
    if (jQuery('.video-js').length) {
        var myPlayer = videojs('featured_video');
        var isPlaying = !myPlayer.paused();

        if (isPlaying) {
            myPlayer.pause();
            jQuery('.hide-on-play').show();
            jQuery('.play-button').removeClass('playing');
        }

    }
    var headerHeight = jQuery('header').height();
    jQuery('html, body').animate({
        'scrollTop': $(this).closest(".block").next().position().top - headerHeight
    })
});

function moveBackground(current) {
    position = 0;
    var current = current;
    interval = setInterval(function () {
        position -= 1;
        current.find('.underlay-in').css({"background-position": +position + "px bottom"})
    }, 50);
}

jQuery('.portfolio-archive-in .item').on('mouseenter', function () {
    active = jQuery(this);
    moveBackground(active);
});

jQuery('.portfolio-archive-in .item').on('mouseleave', function () {
    clearInterval(interval);
});

jQuery('.services-in .service').on('mouseenter', function () {
    active = jQuery(this);
    moveBackground(active);
});

jQuery('.services-in .service').on('mouseleave', function () {
    clearInterval(interval);
});

jQuery('.filter-select').on('click', function () {
    var dd = jQuery(this).parent().find('.filter-dropdown');
    jQuery(this).toggleClass('dd-open');
    dd.slideToggle();
});

jQuery('.filter-item').on('click', function () {
    var dd = jQuery(this).parent();
    jQuery(this).toggleClass('dd-open');
    dd.slideToggle();
});

//LOADER
function showLoader() {
    jQuery('body').addClass('loading').addClass('loading-inline');
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    if (typeof requestAnimationFrame !== 'undefined') {
        window.requestAnimationFrame(function () {
        });
    }
}

function hideLoader() {
    jQuery('body').removeClass('loading').removeClass('loading-inline');
    jQuery('.loader-div').fadeOut(150);
}

var ignore_onbeforeunload = false;
jQuery('a[href^=mailto], a[href^=tel]').on('click', function () {
    ignore_onbeforeunload = true;
});

function showPage(e) {
    if (typeof e !== 'undefined' && typeof e.persisted !== 'undefined' && e.persisted) {
        window.location.reload();
    }
    hideLoader();
}

function hidePage(e) {
    if (!ignore_onbeforeunload) {
        showLoader();
    }
    ignore_onbeforeunload = false;
}

window.addEventListener("pageshow", showPage, false);
jQuery(window).on('load', showPage);

window.addEventListener("pagehide", hidePage, false);
jQuery(window).on('beforeunload', hidePage);
jQuery(window).on('unload', hidePage);


//VIDEOPLAYER


function removeVideoOnSize() {
    if (jQuery(window).width() < 480) {
        if (jQuery('.featured-media-video video')) {
            jQuery('.featured-media-video').html("");
        }

    }
}


jQuery('.play-button').on("click", function () {
    var myPlayer = videojs('featured_video');
    var isPlaying = !myPlayer.paused();


    if (isPlaying) {
        myPlayer.pause();
        jQuery('.hide-on-play').show();
        jQuery('.play-button').removeClass('playing');
    }
    else {
        myPlayer.play();
        jQuery('.hide-on-play').hide();
        jQuery('.play-button').addClass('playing');
    }
});

jQuery('.block-youtube-in .item .icon__play-full').on("click", function () {
    var playerId = jQuery(this).parents('.item').attr('data-video-id');
    var myPlayer = videojs(playerId);
    var isPlaying = !myPlayer.paused();


    if (isPlaying) {
        myPlayer.pause();
        jQuery(this).show();
    }
    else {
        myPlayer.play();
        jQuery(this).hide();
    }
});

jQuery('.sound-button').on("click", function () {
    var myPlayer = videojs('featured_video');
    var currentVolume = myPlayer.muted();

    if (currentVolume) {
        myPlayer.muted(false);
        jQuery('.sound-button').addClass('unmuted');
    }
    else {
        myPlayer.muted(true);
        jQuery('.sound-button').removeClass('unmuted');
    }
});

if (jQuery('.featured-media-video').hasClass('has-autoplay')) {
    setTimeout(function () {
        var myPlayer = videojs('featured_video');
        myPlayer.play();
        jQuery('.hide-on-play').hide();
        jQuery('.play-button').addClass('playing');
    }, 2500);
}

jQuery('.portfolio-filters .filter-dropdown .filter-item').on("click", function () {
    var new_service = jQuery(this).data('filter-item');
    var new_active = jQuery(this).parents('.portfolio-archive-top').find('.active-filters .active-filter[data-filter-active=' + new_service + ']');
    var all_actives = [];
    new_active.toggle();
    new_active.toggleClass('filter-active');

    jQuery('.active-filters .active-filter.filter-active').each(function () {
        all_actives.push(jQuery(this).attr('data-filter-active'));
    });

    toggleItemActive(all_actives);

});

jQuery('.active-filters .active-filter').on("click", function () {
    var all_actives = [];
    jQuery(this).toggle();
    jQuery(this).toggleClass('filter-active');

    jQuery('.active-filters .active-filter.filter-active').each(function () {
        all_actives.push(jQuery(this).attr('data-filter-active'));
    });

    toggleItemActive(all_actives);
});

function initItemActive() {
    var all_actives = [];
    jQuery('.active-filters .active-filter.filter-active').each(function () {
        all_actives.push(jQuery(this).attr('data-filter-active'));
    });
    toggleItemActive(all_actives);
}

function toggleItemActive(all_actives) {
    var items = jQuery('.block-portfolio-archive .item');

    items.each(function (index, value) {
        var element = jQuery(this);
        var item_services_string = jQuery(this).attr('data-service-name');
        element.removeClass('active');
        if (all_actives.length == 0) {
            element.addClass('active');
        } else {
            jQuery.each(all_actives, function (index, value) {
                if (item_services_string.indexOf(value) !== -1) {
                    element.addClass('active');
                }
            });
        }
    });
}

function addGradientToForm() {
    var gradient = jQuery('.footer-form .footer-form-left').attr('data-form-gradient');
    jQuery('.wpcf7-form .input-row span').addClass(gradient);
    jQuery('.wpcf7-form p button').addClass(gradient);
}

function setHeaderColor() {
    if ($(window).scrollTop() > 40) {
        $("header").addClass("black");
    } else {
        $("header").removeClass("black");
    }
}

function imageSlider(){
    $('.featured-media-slider .slider-item:gt(0)').hide();
    setInterval(function(){
            $('.featured-media-slider :first-child').fadeOut()
                .next('.slider-item').fadeIn()
                .end().appendTo('.featured-media-slider');},
        3000);
}