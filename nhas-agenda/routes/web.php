<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'EventController@index')->name('agenda');
Route::get('/agenda', 'EventController@index')->name('agenda');

Route::middleware('verified')->group(function () {

    Route::get('/event/create', 'EventController@create')->middleware('verified');
    Route::post('/event/create', 'EventController@createPost');

    Route::get('/event/{id}', 'EventController@show');
    Route::get('/event/{id}/signup', 'EventController@signup');
    Route::get('/event/{id}/signoff', 'EventController@signoff');

    Route::post('/event/{id}/conversation/create', 'ConversationController@createPost');

});
