
# Voorbeelden projecten

## NHAS Agenda
    - Dit is een simpele applicatie die ik ontwikkeld heb waarmee ik samen met de mensen waarmee ik airsoft evenementen kunnen inplannen.
    - Op een evenement kan je je dan inschrijven en vervolgens is het mogelijk om te chatten binnen het evenement.
    
## Emma Wagemans
Bekijk hier het portfolio: http://emmawagemans.itsmeijer.nl/

    - Dit was het eindproject voor school waar ik samen met 3 designers, 1 front-end developer en ik als back-end developer een portfolio hebben ontwikkeld voor een make-up artist.
    - Deze site is helaas alleen nog maar op mijn staging te bekijken omdat ze helaas is gestopt met haar werk als make-up artist.
    - Deze site is opgedeeld in blokken, het is mogelijk om op elke pagina elke blok neer te zetten. De volgorde kan hiermee dus ook veranderd worden.
    - Ook hadden we het mogelijk gemaakt om te kiezen uit een kleuren selectie, dit betekent dat het mogelijk is om de kleuren op de site aan te passen naar vooraf gedefinieerde kleuren paletten.
    
    
    