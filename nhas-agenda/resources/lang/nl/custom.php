<?php

return [
    'email' => 'E-mailadres',
    'password' => 'Wachtwoord',
    'confirmpassword' => 'Wachtwoord opnieuw',
    'rememberme' => 'Gegevens onthouden',
    'login' => 'Inloggen',
    'logout' => 'Uitloggen',
    'register' => 'Registreren',
    'forgot' => 'Wachtwoord vergeten?',
    'resetpassword' => 'Wachtwoord wijzigen',
    'name' => 'Naam',
    'sendresetemail' => 'Verstuur wachtwoord wijzigings e-mail',
    'resetemailsend' => 'Er is een verificatie link verstuurd naar je e-mailadres',
    'resetbeforeproceed' => 'Voor je verder gaat, check je e-mail voor een verificatie link.',
    'resetnotreceived' => 'Heb je geen e-mail ontvangen?',
    'resetrequestnew' => 'Klik hier om er nog een aan te vragen',

    'agenda' => 'Agenda',
    'create_event' => 'Evenement aanmaken',
    'conversation' => 'Conversatie',
    'send' => 'Verstuur',
    'signup' => 'Inschrijven',
    'signoff' => 'Uitschrijven',

    'location' => 'Locatie',
    'start_date' => 'Start datum',
    'end_date' => 'Eind datum',

];
