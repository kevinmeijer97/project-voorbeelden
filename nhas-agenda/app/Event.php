<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User','event_users');
    }

    public function getUsers() {
        return $this::with('users')->get();
    }

    public function conversations() {
        return $this->hasMany('App\Conversation')->orderBy('created_at', 'DESC');
    }
}
