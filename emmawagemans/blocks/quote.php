<?php
$text = get_sub_field('text');
if(!empty($text)){
    ?>
    <div class="block block-quote">
        <div class="wrapper">
            <div class="quote-content">
                <span class="line-top gradient"></span>
                <p><?=$text?></p>
                <span class="line-bottom gradient"></span>
            </div>
        </div>
    </div>
    <?php
}