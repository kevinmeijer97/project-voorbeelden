<?php
$videos = get_sub_field('videos');
if(!empty($videos)){
    ?>
    <div class="block block-youtube">
        <div class="wrapper">
            <div class="block-youtube-in">
                <?php
                $i = 0;
                foreach($videos as $video){
                    $link = $video['youtube_link'];
                    $title = $video['title'];
                    $image = $video['image'];
                    if(!empty($link)){
                        ?>
                        <div class="item" data-video-id="tutorial_video-<?=$i?>">
                            <?=!empty($title) ? "<span class=\"text-title\">{$title }</span>" : ""?>
                            <div class="item-in">
                                <video id="tutorial_video-<?=$i?>"
                                       class="video-js vjs-default-skin"
                                       muted
                                       controls
                                       poster="<?=get_custom_src_of_imagefield($image, 'services')?>"
                                       width="600" height="400"
                                       data-setup='{ "techOrder": ["youtube", "html5"], "sources": [{ "type": "video/youtube", "src": "<?=$link?>"}] }'>
                                </video>
                                <img src="<?=get_custom_src_of_imagefield($image, 'small')?>" alt="">
                                <i class="icon icon__play-full"></i>
                            </div>
                        </div>
                        <?php
                    }
                    $i++;
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}
?>

