<?php
$has_breadcrumbs = get_sub_field('has_breadcrumbs');
$image = get_sub_field('image');
$has_small_wrapper = get_sub_field('has_small_wrapper');
if(!empty($image)){
    $title = get_sub_field('title');
    ?>
    <div class="block block-title" style="background-image: url('<?=get_custom_src_of_imagefield($image, 'large')?>');">
        <div class="wrapper<?=$has_small_wrapper ? " wrapper__small" : ""?>">
            <div class="gradient"></div>
            <?php
            if($has_breadcrumbs){
                if(function_exists('yoast_breadcrumb')){
                    $breadcrumbs = yoast_breadcrumb('<div class="breadcrumbs">', '</div>');
                    echo $breadcrumbs;
                }
            }
            echo !empty($title) ? "<div class=\"title-in\"><h1 class=\"title-small\">{$title}</h1></div>" : "";
            ?>
        </div>
    </div>
    <?php
}