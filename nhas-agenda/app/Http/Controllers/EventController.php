<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventUser;
use App\Conversation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('verified');
    }

    public function index()
    {

        $events = Event::orderBy('start_date')->get();

        return view('agenda')->with(['events' => $events]);
    }


    public function create()
    {
        return view('events.create');
    }

    public function createPost(Request $request)
    {
        $eventData = $request->validate([
            'event_location' => 'required',
            'event_start_date' => 'required',
            'event_end_date' => '',
        ]);

        $event = new Event();
        $event->location = $eventData['event_location'];
        $event->start_date = $eventData['event_start_date'];
        $event->end_date = $eventData['event_end_date'];
        $event->save();

        $user = auth()->user();

        $eventUser = new EventUser();
        $eventUser->user_id = $user->id;
        $eventUser->event_id = $event->id;
        $eventUser->save();

        return redirect('agenda');
    }

    public function show($id)
    {
        $event = Event::find($id);

        $conversations = $event->conversations;

        return view('events.show')->with(['event' => $event, 'conversations' => $conversations]);
    }

    public function signup($id)
    {

        $event = Event::find($id);
        $user = auth()->user();

        $eventUser = new EventUser();
        $eventUser->user_id = $user->id;
        $eventUser->event_id = $event->id;
        $eventUser->save();


        return redirect('event/' . $event->id);
    }

    public function signoff($id)
    {

        $event = Event::find($id);
        $user = auth()->user();

        $eventUser = EventUser::where('user_id', $user->id)->where('event_id', $event->id)->first();
        $eventUser->delete();


        return redirect('event/' . $event->id);
    }
}
