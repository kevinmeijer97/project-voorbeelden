<div class="block block-basic-page">
    <div class="wrapper wrapper__small">
        <?php
        if(have_rows('content')){
            while(have_rows('content')){
                the_row();
                $block = str_replace(['content_', '_'], ['', '-'], get_row_layout());
                get_template_part('blocks/content/' . $block);
            }
        }
        ?>
    </div>
</div>