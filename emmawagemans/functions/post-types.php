<?php 

function create_post_type_itsmeijer(){
        // ALWAYS USE SINGLE NAMES
        register_post_type('project',
            array(
                'labels'              => getPostTypeLabels("Project", "Portfolio"),
                'public'              => true,
                'hierarchical'        => true,
                'has_archive'         => 'projects',
                'exclude_from_search' => false,
                'show_in_admin_bar'   => true,
                'show_in_nav_menus'   => true,
                'publicly_queryable'  => true,
                'query_var'           => true,
                'menu_icon'           => 'dashicons-camera', //https://developer.wordpress.org/resource/dashicons/
                'supports'            => array(
                    'title',
                    //'editor',
                    //'excerpt',
                    //'thumbnail',
                    //'custom-fields'
                ),
                'can_export'          => true,
                'taxonomies'          => array(
                    //'post_tag',
                    //'category'
                )
            )
        );


        $args = array(
            'hierarchical'      => true,
            'label'             => "Diensten",
            'show_ui'           => true,
            'show_in_menu'      => true,
            'show_in_nav_menus' => true,
            'show_admin_column' => true,
            'public'            => true,
            'query_var'         => true,
        );

        register_taxonomy('project_services', array('project'), $args);
}

