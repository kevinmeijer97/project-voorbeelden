<?php
$title = get_sub_field('title');
$type = get_sub_field('projects_type');
$pattern_variant = get_sub_field('pattern_variant');
if($type == "manual"){
    $projects = get_sub_field('projects');
}else{
    $args = array(
        'post_type'      => 'project',
        'post_status'    => 'publish',
        'orderby'        => 'date',
        'order'          => 'DESC',
        'posts_per_page' => 4,
    );

    $projects = get_posts($args);
}
if(!empty($projects)){
    ?>
    <div class="block block-featured-images">
        <div class="wrapper">
            <?php
            if(is_404()) {
                $title = get_field('text_featured_projects', 'option');
            }
            echo !empty($title) ? "<div class=\"featured-images-top\"><h2 class=\"title\">{$title}</h2></div>" : "";
            ?>
            <div class="featured-images">
                <div class="underlay gradient">
                    <div class="underlay-in" style="background-image: url('<?=get_template_directory_uri()?>/assets/images/pattern-<?=$pattern_variant?>.svg')"></div>
                </div>
                <?php
                foreach($projects as $post){
                    setup_postdata($post);
                    get_template_part('loops/featured-project');
                }
                wp_reset_postdata();
                ?>
                <div class="clear"></div>
            </div>
            <?php
            $has_button = get_sub_field('has_button');
            if($has_button){
                $button = get_sub_field('button');
                if(!empty($button['title']) && !empty($button['url'])){
                    ?>
                    <div class="featured-images-button">
                        <a href="<?=$button['url']?>"<?=(!empty($button['target']) ? " target=\"{$button['target']}\"" : "")?> class="btn btn-gradient"><?=$button['title']?>
                            <i class="icon icon__arrow-white"></i>
                        </a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <?php
}