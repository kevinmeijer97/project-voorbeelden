<?php
global $post;


$terms = get_the_terms($post, 'project_services');
$categories = [];
foreach($terms as $term){
    $categories[] = $term->slug;
}

if(in_array($_GET['filter-services'], $categories) || empty($_GET['filter-services'])){
    $image = get_field('archive_image');
    $subtitle = get_field('project_subtitle');
    $date = get_field('project_date');
    $newDate = explode('/', $date);

    ?>
    <a href="<?=get_permalink();?>" target="_self" class="item active" <?=!empty($categories) ? "data-service-name=\"" . (implode(',', $categories)) . "\"" : ""?>>
        <div class="item-in">
            <div class="shadow"></div>
            <img src="<?=get_custom_src_of_imagefield($image, 'archive_project')?>" alt="">
            <?php
            if(!empty($newDate)){
                $day = $newDate[0];
                $month = $newDate[1];
                $year = $newDate[2];
                ?>
                <div class="item-date"><?=$day?> <?=$month?><span><?=$year?></span></div>
                <?php
            }
            ?>
            <h3 class="item-title"><?=$post->post_title?>
                <?=!empty($subtitle) ? "<span>{$subtitle}</span>" : ""?>
            </h3>
            <div class="underlay gradient">
                <div class="underlay-in" style="background-image: url('<?=get_template_directory_uri()?>/assets/images/pattern-triangles.svg')"></div>
            </div>
        </div>
    </a>
    <?php
}
