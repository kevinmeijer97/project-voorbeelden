## NHAS Agenda
    - Dit is een simpele applicatie die ik ontwikkeld heb waarmee ik samen met de mensen waarmee ik airsoft evenementen kunnen inplannen.
    - Op een evenement kan je je dan inschrijven en vervolgens is het mogelijk om te chatten binnen het evenement.
    - Interessante bestanden zijn:
        - database/migrations/
        - In /app/ de models en daarin in /app/http/controllers/ de bijbehorende controllers.
